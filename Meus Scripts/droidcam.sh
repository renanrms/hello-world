# Instalação

sudo apt install linux-headers-`uname -r` gcc make

cd /tmp/
wget https://files.dev47apps.net/linux/droidcam_latest.zip
unzip droidcam_latest.zip -d droidcam
cd droidcam

sudo ./install-client

sudo ./install-video

sudo ./install-sound


# Pode ser necessário para conexões USB.

sudo apt-get install adb


# Necessário para ver o dispositivo de som listado.

pacmd load-module module-alsa-source device=hw:Loopback,1,0

##  Para desfazer

pacmd unload-module module-alsa-source


# Para verificar se o dispositivo de vídeo (e creio que de áudio também) está carregado.

lsmod | grep v4l2loopback_dc

# Opção no menu

echo -e ‘[Desktop Entry]\n Version=1.0\n Name=DroidCam\n Exec=droidcam\n Icon=droidcam\n Type=Application\n Categories=Application’ | sudo tee /usr/share/applications/droidcam.desktop
