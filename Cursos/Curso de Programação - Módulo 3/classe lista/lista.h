#include<string>


class lista {

  private:
    typedef
    struct elemento {
      int valor;
      elemento* pProximo;
      elemento* pAnterior;
    } tipoElemento;

  public:
    lista ();
    lista (const lista &original);
    //int ler (int posicao);
    //int index (int valor); // Retorna a posição do elemento específicado, ou -1.
    void push (int valor); // adiciona ao final.
    void inserir (int valor, int posicao);
    lista fatia (int passo = 1) const;
    lista fatia (int inicial, int final, int passo = 1) const;
    std::string toString (int inicial = 0, int final = -1) const;
    int lenth () const;

    // Operadores de coerção
    operator char* () const;

    // Sobrecarga de operadores
    void operator= (const lista original);

  private:
    tipoElemento* pInicio;
    tipoElemento* pFim;
    int listLenth;
};
