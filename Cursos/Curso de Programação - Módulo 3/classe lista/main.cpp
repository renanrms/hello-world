#include "lista.h"

#include <iostream>
#include <string>

using namespace std;

int main (int argc, char *argv[]) {
  lista minhaLista = lista();

  cout << "Lista vazia: " << minhaLista << endl;

  minhaLista.push (10);
  minhaLista.push (20);
  minhaLista.push (50);
  cout << "Inserindo 10, 20 e 50: " << minhaLista << endl;

  minhaLista.inserir (30, 2);
  minhaLista.inserir (40, 3);
  cout << "Inserindo 30 e 40 antes do 50: " << minhaLista << endl;
  cout << "Convertendo para string explicitamente: "
       << minhaLista.toString() << endl;
  cout << "Convertendo com fatiamento do índice 1 ao 3 (inclusive): "
       << minhaLista.toString(1,3) << endl;
  cout << "Fatiando de 0 a 2 (inclusive): " << minhaLista.fatia(0, 2) << endl;
  cout << "Fatiando a lista inteira com passo -1: "
       << minhaLista.fatia(-1) << endl;
  cout << "Comprimento: " << minhaLista.lenth() << endl;

  lista segundaLista;
  segundaLista = minhaLista;
  cout << "Copia da lista anterior: " << segundaLista << endl;

  minhaLista.inserir (0, 0);
  minhaLista.inserir (15, 2);
  minhaLista.push (60);
  cout << "Inserindo 0 no início, 15 no meio e 60 no final da primeira lista: "
       << minhaLista << endl;
  cout << "Segunda lista após a alteração da primeira: " << segundaLista << endl;

  return 0;
}
