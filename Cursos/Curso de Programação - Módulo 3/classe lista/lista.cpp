#include "lista.h"

#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

lista::lista () {
  this->pInicio = NULL;
  this->pFim = NULL;
  this->listLenth = 0;
}

lista::lista (const lista &original) {
  this->pInicio = NULL;
  this->pFim = NULL;
  this->listLenth = 0;

  tipoElemento* pElemento = original.pInicio;

  while (pElemento != NULL) {
    this->push (pElemento->valor);
    pElemento = pElemento->pProximo;
  }
}

void lista::push (int valor) {
  this->inserir (valor, this->listLenth);
}

void lista::inserir (int valor, int posicao) {
  tipoElemento* pProximo = NULL;
  tipoElemento* pElemento = NULL;
  tipoElemento* pAnterior = NULL;
  int posicaoAtual;

  if (posicao < 0) { posicao = listLenth + posicao; }

  if (posicao < 0 || posicao > this->listLenth) {
    /*exceção: índice fora do intervalo.*/
  }
  else if (posicao <= (this->listLenth-1)/2) {
    pProximo = this->pInicio;
    posicaoAtual = 0;

    while (posicaoAtual < posicao && pProximo != NULL) {
      pAnterior = pProximo;
      pProximo = pProximo->pProximo;
      posicaoAtual++;
    }
  }
  else {
    pAnterior = this->pFim;
    posicaoAtual = this->listLenth;

    while (posicaoAtual > posicao && pAnterior != NULL) {
      pProximo = pAnterior;
      pAnterior = pAnterior->pAnterior;
      posicaoAtual--;
    }
  }

  pElemento = (tipoElemento*) malloc (sizeof(tipoElemento)); //Adicionar exceção.

  pElemento->valor = valor;
  pElemento->pProximo = pProximo;
  pElemento->pAnterior = pAnterior;

  if (pAnterior == NULL) { this->pInicio = pElemento; }
  else { pAnterior->pProximo = pElemento; }

  if (pProximo == NULL) { this->pFim = pElemento; }
  else { pProximo->pAnterior = pElemento; }

  this->listLenth++;
}

lista lista::fatia (int inicial, int final, int passo) const {
  tipoElemento* pElemento;
  int posicaoAtual;
  lista subLista = lista();

  // Adaptação para indexação a partir do final com números negativos.
  if (inicial < 0) { inicial = this->listLenth + inicial; }
  if (final   < 0) { final   = this->listLenth + final;   }

  if ((inicial < 0 || inicial > (this->listLenth - 1))
  ||  (final   < 0 || final   > (this->listLenth - 1))) {
    /*exceção: índice fora do intervalo.*/
  }

  if (inicial == final) {
    return subLista;
  }

  if (passo == 0) {
    /*exceção: passo igual a zero.*/
  }

  if ((passo < 0 && inicial < final)
  ||  (passo > 0 && inicial > final)) {
    /*exceção: passo direcionado para fora do intervalo.*/
  }

  // encontra o elemento dado pelo índice inicial.
  pElemento = this->pInicio;
  posicaoAtual = 0;
  while (posicaoAtual < inicial) {
    pElemento = pElemento->pProximo;
    posicaoAtual++;
  }

  while ((posicaoAtual >= final && passo < 0) ||
         (posicaoAtual <= final && passo > 0)) {

    subLista.push(pElemento->valor);

    int count = passo;
    while (count > 0 && pElemento != NULL) {
      pElemento = pElemento->pProximo;
      count--;
    }
    while (count < 0 && pElemento != NULL) {
      pElemento = pElemento->pAnterior;
      count++;
    }

    posicaoAtual += passo;
  }

  return subLista;
}

lista lista::fatia (int passo) const {
  if (passo > 0) {
    return this->fatia(0, this->listLenth - 1, passo);
  }
  else {
    return this->fatia(this->listLenth - 1, 0, passo);
  }
}

string lista::toString (int inicial, int final) const {
  tipoElemento* pElemento = this->pInicio;
  int contador = 0;
  string listaToString = string();

  if (inicial < 0) { inicial = listLenth + inicial; }
  if (final < 0) { final = listLenth + final; }

  while (pElemento != NULL && contador < inicial) {
    pElemento = pElemento->pProximo;
    contador++;
  }

  listaToString += "[";

  if (pElemento != NULL && contador <= final) {
    listaToString += to_string(pElemento->valor);
    pElemento = pElemento->pProximo;
    contador++;
  }

  while (pElemento != NULL && contador <= final) {
    listaToString += ", " + to_string(pElemento->valor);
    pElemento = pElemento->pProximo;
    contador++;
  }

  listaToString += "]";

  return listaToString;
}

int lista::lenth () const {
  return this->listLenth;
}

lista::operator char* () const {
  string listaToString = this->toString();
  char* charArray = new char[listaToString.size() + 1];

  std::copy(listaToString.begin(), listaToString.end(), charArray);
  charArray[listaToString.size()] = '\0';

  return charArray;
}

void lista::operator= (const lista original) {
  tipoElemento* pElemento = original.pInicio;

  while (pElemento != NULL) {
    this->push (pElemento->valor);
    pElemento = pElemento->pProximo;
  }
}
