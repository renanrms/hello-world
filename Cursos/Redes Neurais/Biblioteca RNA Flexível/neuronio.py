from derivada import derivate


class Neuronio:
	""" 
	Um neurônio para contrução de redes neurais de qualquer arquitetura. 
	  
	Attributes: 
		_backward (list, int): Lista com os objetos Neurônio conectados à entrada deste, ou quantidade de conexões de entrada do neurônio (para quando for um neurônio de entrada). 
		_forward (list, int): Lista com os objetos Neurônio conectados à saída deste, ou None (para quando for um neurônio de saída). 
		_activation (function): função de ativação. 
		_bias (bool): True se houver bias no neurônio e False no caso contrário. 
		_inputs (list): Valores de entrada do neurônio. 
		_output (float): Valor de saída do neurônio. 
		_weights (list): Pesos do neurônio. 
		_errors (list): Erros nas saídas do neurônio. No caso de não ser um neurônio de saída é um erro retropropagado. 
		_delta (float): Delta do neurônio usado no backpropagation. 
	"""

	def __init__(self, backward, forward, activation, bias=True):
		self._backward = backward
		self._forward = forward
		self._activation = activation
		self._bias = bias

		if (type(backward) == int):
			self._weights = [1]*(backward + int(bias))
		else:
			self._weights = [1]*(len(backward) + int(bias))

		self._inputs = None
		self._output = None
		self._errors = None
		self._delta = None

	def set_inputs(self, inputs=None):
		if (type(self._backward) == int):
			if (len(inputs) != self._backward):
				raise Exception('O comprimento da lista não está compatível com as entradas da rede.')
			_inputs = inputs
		else:
			# Pegar as entradas a partir dos neurônios conectados a este.
			pass

	def backpropagate_delta(self, ):
		"""
		Propaga o delta para os neurônios conectados à entrada deste.
		Atualiza a variável errors de outro neurônio com o delta deste.
		"""
		pass

	def set_errors(self, error=None):
		"""
		Caso o vetor de erros não esteja atualizado no ciclo, utiliza o método backpropagate_delta dos neurônios conectados à saída deste para atualizar os erros.
		"""

	def calculate_output(self, ):
		"""
		Calcula a saída do neurônio.
		"""
		pass

	def calculate_delta(self, ):
		"""
		Calcula o delta do neurônio, usado no algoritmo de backpropagation.
		"""
		pass
