#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Funções para serem utilizadas como funções de ativação em redes neurais artificiais.

Variáveis:
	tanh: referência para função tanh da biblioteca math.

Funções:
	sigmoid(float) -> (float)
	step(float) -> (float)
	retifier(float) -> (float)
	sigmoid_with_coeficient(float) -> (function)
	tanh_with_coeficient(float) -> (function)
	leaky_with_coeficient(float) -> (function)
"""

import math


tanh = math.tanh

def tanh_with_coeficient(a):
	"""
	Retorna uma função tangente hiperbólica com o coeficiente especificado.
	"""
	def tanh_a(x):
		return ( (math.e)**(2*a*x) - 1 )/( (math.e)**(2*a*x) + 1 )
	return tanh_a

def sigmoid(x):	
	"""
	Retorna a sigmoid de x.
	"""
	return 1/( 1 + (math.e)**(-x) )

def sigmoid_with_coeficient(a):
	"""
	Retorna uma função sigmoide com o coeficiente especificado.
	"""
	def sigmoid_a(x):
		return 1/( 1 + (math.e)**(-a*x) )
	return sigmoid_a

def step(x):
	"""
	Retorna u(x), função degrau.
	"""
	if (x >= 0):
		return 1
	else:
		return 0

def retifier(x):
	"""
	Retorna max(0, x).
	"""
	return max(0, x)

def leaky_with_coeficient(a):
	"""
	Retorna uma função leaky com o coeficiente especificado.
	"""
	def leaky_a(x):
		if (x >= 0):
			return x
		else:
			return a*x
	return leaky_a
