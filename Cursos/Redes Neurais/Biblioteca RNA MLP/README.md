# Biblioteca RNA MLP

Biblioteca em Python para contruir Redes Neurais Artificiais do tipo Multi-Layer Perceptron, com calculos feitos através da biblioteca numpy. A melhor maneira de conhecer é olhar o arquivo de exemplo (test.py) e depois o arquivo principal (MLPClassifier.py) para entender como funciona.

**NOTA**: O método de atualização dos pesos ainda não foi implementado, logo a rede não está aprendendo, apenas utilizando os pesos iniciais, que podem ser definidos com valor fixo ou aleatório. Apesar de não aprender, ainda é preciso usar o método fit() antes do predict() para inicializar os pesos. O exemplo mostra bem essa utilização.

Esta pasta é composta dos seguintes arquivos:

* **MLPClassifier.py**: Módulo principal, com a classe da rede neural e é o único que deve ser importado essencialmente.
* **activation.py**: Módulo com as funções de ativação implementadas, também deve ser importado para ter acesso a todas as funções e utilizar além daquela que o módulo principal toma como default.
* **derivada.py**: Função de derivação numérica que foi implementada para que se possa realizar os cálculos de backpropagation.
* **example.py**: Módulo com um exemplo, que imprime na tela vários dados (pesos, arrays, etc) para visualizar o que a rede fez.
* **test.py**: Módulo de testes unitários automatizados. Tem apenas um teste muito simples, que foi útil para o desenvolvimento.
