#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Rede Neural Artificial do tipo Multi-Layer Perceptron.

Classes:

	MLPClassifier
"""

# Imports externos
import numpy as np
import random

# Imports do programa
import derivada
import activation

class MLPClassifier:
	"""
	Rede Neural do tipo Multi-Layer Perceptron para problemas de classificação de variável binária.
	Para classificação com mais de duas classes as classes devem ser divididas em colunas de maneira que todas as colunas de saída sejam binárias.
	
	Parameters:

		hidden_layer_sizes (tupla): Uma tupla com os tamanhos das camadas escondidas.
		activation (function): Função de ativação a ser usada na rede. Uma função de uma variável real para uma variável real.
		alpha (float): Coeficiente de decaimento.
		batch_size (int): tamanho dos blocos de treinamento.
		initial_weights (int/float, str): Pode ser o valor inicial dos pesos da rede ou uma string informando o método, como 'random'.
	"""
	_hidden_layer_sizes = None
	_activation = None
	_alpha = None
	_batch_size = None
	_initial_weights = None
	_weights = None

	def __init__(self, hidden_layer_sizes=(), activation=activation.tanh, alpha=0.0001, batch_size=200, initial_weights=0.0):
		"""
		Cria o obtjeto MLPClassifier.
		"""
		self._hidden_layer_sizes = hidden_layer_sizes
		self._activation = activation
		self._alpha = alpha
		self._batch_size = batch_size
		self._initial_weights = initial_weights

	def fit(self, x, y):
		"""
		Treina a rede com os dados de entrada 'x' e de saída 'y'.

		Parameters:

			x (np.Array): Entrada.
			y (np.Array): Saída.
		"""
		# Configura os arrays de camadas.
		layer_sizes = (x.shape[1],) + self._hidden_layer_sizes + (y.shape[1],)
		
		self.initialize_weights(layer_sizes)

		y_pred = np.empty( y.shape )

		# Calcula a saída por bach, calcula as médias de entradas por camada, calcula os erros e atualiza os pesos.
		batchs = list(range(0, x.shape[0], self._batch_size))
		for i in range(len(batchs)):
			begin = batchs[i]
			if (i+1 < len(batchs)):
				end = batchs[i+1]
			else:
				end = x.shape[0]
			
			# Calcula as saídas
			y_pred[begin:end, : ] = self.predict( x[begin:end, : ] )
			
			# Calcula os erros
			errors = self.errors(y_pred, y)

			# Atualiza os pesos.
			self.update_weights(errors)

	def predict(self, x):
		"""
		Calcula as saídas para a entreda 'x' com a rede pré-treinada.

		Parameters:

			x (np.Array): Entrada.

		Returns:

			Saída da rede. (np.Array)
		"""

		def concat_ones(array):
			"""Concatena uma coluna de 1's a um array numpy para representar o bias."""
			return np.concatenate((array, np.ones( (array.shape[0], 1) )), axis=1, out=None)

		output = x

		for layer in self._weights[0:-1]:

			# Aplica a matriz de pesos à entrada da camada.
			output = np.dot(concat_ones(output), layer)

			# Aplica a função de ativação.
			output = np.vectorize(self._activation)(output)

		# Aplica a matriz de pesos da última camada e a função de ativação step para binarizar a saída.
		output = np.dot(concat_ones(output), self._weights[-1])
		output = np.vectorize( activation.step )(output)

		return output
	
	def errors(self, y_pred, y):
		"""
		Retorna um array numpy com os erros médios de cada de y_pred em relação a y.
		O propósito deste método é servir para o cálculo do backpropagation e não como métrica de erro.
		"""
		errors = y - y_pred
		errors = errors.mean(axis=0)

		return errors

	def update_weights(self, errors):
		"""
		Realiza um ciclo de atualização dos pesos da rede a partir do vetor de erros passado como argumento.
		"""
		pass

	def initialize_weights(self, layer_sizes):
		"""
		Inicializa os pesos com a regra definida no atributo _initial_weights.
		"""
		self._weights = []
		
		# Aloca os arrays de pesos colocando uma linha a mais devido ao termo bias.

		for i in range(len(layer_sizes) - 1):
			weight_array = np.empty( (layer_sizes[i] + 1, layer_sizes[i+1]) )
			self._weights.append( weight_array )

		# Define os valores dos pesos de acordo com o valor passado.

		for i in range(len(self._weights)):

			random.seed()

			if (type(self._initial_weights) == int or type(self._initial_weights) == float):

				self._weights[i] = np.vectorize( lambda x: self._initial_weights )( self._weights[i] )

			elif (self._initial_weights == 'random'):

				self._weights[i] = np.vectorize( lambda x: random.gauss(0, 0.1) )( self._weights[i] )

