#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Função derivada.

Funções:
	derivate(function) -> (function)
"""

def derivate(f, dx=0.000001):
	'''
	Retorna a função 'f' derivada com um passo de derivação 'dx'.

	Parameters:
	
	f (function): Uma função de uma variável real.
	dx (float): Passo da derivação.

	Returns:

		function: Derivada da função passada como argumento. Esta função pode sua vez recebe e retorna apenas um parâmetro do tipo float.
	'''
	def f_derivate(x):
		return ( f(x + dx/2) - f(x - dx/2) )/(dx)
	return f_derivate

if __name__ == '__main__':

	# Pequeno teste com a função "dobro".
	def dobro(a):
		return 2*a

	df_dx = derivate(dobro)

	for x in [1, 2, 5, 10, 1000]:
		print(f'A derivada da função f:f(x)=2x em x={x} é: {df_dx(1)}')