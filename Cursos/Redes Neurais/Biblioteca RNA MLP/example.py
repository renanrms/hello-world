#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np

from MLPClassifier import MLPClassifier


x = np.ones((10, 2))
y = np.ones((10, 1))

model = MLPClassifier(hidden_layer_sizes=(4, 4, 4), initial_weights='random')
# model = MLPClassifier(hidden_layer_sizes=(4, 4, 4), initial_weights=2)
# model = MLPClassifier(hidden_layer_sizes=(4, 10, 5, 4), initial_weights=2)
# model = MLPClassifier()

model.fit(x, y)

print('\nDimensões de todas as matrizes de pesos, incluindo das camadas de entrada e saída:')
print( [matrix.shape for matrix in model._weights] )

print('\nMatrizes de pesos:')
print(model._weights)

y_pred = model.predict(x)
print('\ny_pred:')
print(y_pred)

errors = model.errors(y_pred, y)
print('\nErros de predição médios por coluna de y_pred:')
print(errors)
