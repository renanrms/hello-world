#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest

import numpy as np

from MLPClassifier import MLPClassifier


class Test_MLPClassifier(unittest.TestCase):
    def test_fit_predict(self):
        x = np.ones((10, 2))
        y = np.ones((10, 1))

        model = MLPClassifier(hidden_layer_sizes=(4, 4, 4), initial_weights='random')

        model.fit(x, y)

        model.predict(x)

if __name__ == "__main__":
    unittest.main()
