% Determina o vetor K para realimentação de estado do sistema,
% fazendo a alocação desejada dos polos.

% Entradas
A = [ -13 5 ; -30 12 ];
B = [1 3]';
desired_poles = [-2 -3];

% Procedimento
K = acker(A, B, desired_poles);