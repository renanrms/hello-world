% Determina o vetor K = [Kp Ki] para realimenta��o de estado do sistema,
% fazendo a aloca��o desejada dos polos com a��o integral.

% Entradas
A = [ 0 1 ; 4 -1 ];
B = [0 1]';
C = [1 1];
desired_poles = [-4 -8+2i -8-2i];

% Procedimento
[A_m, A_n] = size(A);
[C_m, C_n] = size(C);
A_bar = [A zeros(A_m, 1); C zeros(C_m, 1)];
B_bar = [B ; 0];

K = acker(A_bar, B_bar, desired_poles);