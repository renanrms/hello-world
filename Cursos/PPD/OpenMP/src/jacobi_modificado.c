#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <omp.h>

#define COLUMNS 1000
#define ROWS 1000
#define MAX_TEMP_ERROR 0.01

double *Aaux;
double *A;
double *Anew;
double *Aold;

void iniciar(double *M);

void swap(double **p1, double **p2);

void print_matrix(double *M, int row_i, int row_f, int col_i, int col_f);

int main(int argc, char *argv[])
{
	int i, j;
	int max_iterations = 3000;
	int iteration = 1;
	double dt = 100;

	A = (double *)malloc(sizeof(double) * (COLUMNS + 2) * (ROWS + 2));
	Aaux = (double *)malloc(sizeof(double) * (COLUMNS + 2) * (ROWS + 2));

	/* É preciso inicializar as duas matrizes porque as bosdar ao redos da matriz não são escritas de um passo a outro, então carregariam lixo de memória caso não fossem iniciadas */
	iniciar(A);
	iniciar(Aaux);

	double inicio = omp_get_wtime();

	/* Faz os ponteiros auxiliares para as duas matrizes na memória */
	Aold = A;
	Anew = Aaux;

	while (dt > MAX_TEMP_ERROR && iteration <= max_iterations)
	{
		dt = 0.0;

		#pragma omp parallel for collapse(2) reduction(max : dt)
		for (i = 1; i <= ROWS; i++)
			for (j = 1; j <= COLUMNS; j++)
			{
				Anew[i * COLUMNS + j] = 0.25 * (Aold[(i + 1) * COLUMNS + j] +
												Aold[(i - 1) * COLUMNS + j] +
												Aold[i * COLUMNS + (j + 1)] +
												Aold[i * COLUMNS + (j - 1)]);
				dt = fmax(fabs(Anew[i * COLUMNS + j] - Aold[i * COLUMNS + j]), dt);
			}

		/* 
		 * Apenas para debug e visualização de como está funcionando o método.
		 * Caso queita ver apenas uma parte inicial da matriz, descomente a primeira linha e ajuste os valores.
		 * Caso queita visualizar toda a matriz de temperaturas, decomente a segunda linha (apenas para matrizes pequenas).
		 */
		if (iteration % 10 == 1)
		{
			//print_matrix(Anew, 0, 4, 0, 4);
			//print_matrix(Anew, 0, ROWS + 1, 0, COLUMNS + 1);
		}

		/* Faz a troca dos ponteiros evitando a cópia da nova matriz */
		swap(&Anew, &Aold);

		iteration++;
	}

	/* Como saída deste programa está sendo considerado o ponteiro Anew */

	double fim = omp_get_wtime();

	printf("Erro maximo na iteracao %d era %f. O tempo de execução foi de %f  segundos\n", iteration - 1, dt, fim - inicio);
	return (0);
}

void swap(double **p1, double **p2)
{
	double *paux;

	paux = *p1;
	*p1 = *p2;
	*p2 = paux;
}

void print_matrix(double *M, int row_i, int row_f, int col_i, int col_f)
{
	for (int row=row_i; row<=row_f; row++)
	{
		for (int col=col_i; col<=col_f; col++)
		{
			printf("%.2lf\t", M[row*COLUMNS + col]);
		}
		printf("\n");
	}
	printf("\n");
}

void iniciar(double *M)
{
	int i, j;

	/* Preenche toda a matriz com zeros */
	for (i = 0; i <= ROWS + 1; i++)
	{
		for (j = 0; j <= COLUMNS + 1; j++)
		{
			M[i * COLUMNS + j] = 0.0;
		}
	}

	/* Preenche a equerda da matriz com 0 e a borda direita com um degradê de 0 a 100 */
	for (i = 0; i <= ROWS + 1; i++)
	{
		M[i * COLUMNS + 0] = 0.0;
		M[i * COLUMNS + (COLUMNS + 1)] = (100.0 / ROWS) * i;
	}

	/* Preenche a borda superios com 0 e a borda inferior com um degradê de 0 a 100 */
	for (j = 0; j <= COLUMNS + 1; j++)
	{
		M[0 * COLUMNS + j] = 0.0;
		M[(ROWS + 1) * COLUMNS + j] = (100.0 / COLUMNS) * j;
	}
}
