#include <stdio.h>

int main(int argc, char *argv[])
{ /* acc_private.c  */
    int a[2] = {10, 20}, b[2] = {100, 200}, c = 1000;

    #pragma acc parallel private(a) firstprivate(b) num_gangs(4)
    {
        a[0] += 1;
        b[0] += 1;
        c += 1;
        printf("Valor de a = %d, b = %d, c = %d \n", a[0], b[0], c);
    }
    return 0;
}
