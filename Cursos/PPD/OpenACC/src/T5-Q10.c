#include <stdio.h>

#define SIZE 5000

float a[SIZE][SIZE];
float b[SIZE][SIZE];
float c[SIZE][SIZE];

int main()
{
	int i, j;

	#pragma acc parallel create(a[0:SIZE], b[0:SIZE], c[0:SIZE]) \
	device_type(nvidea) vector_length(1024)
	{
		#pragma acc loop gang tile(1000,1000)
		for (i = 0; i < SIZE; ++i)
		{
			#pragma acc loop vector
			for (j = 0; j < SIZE; ++j)
			{
				a[i][j] = (float)i + j;
				b[i][j] = (float)i - j;
				c[i][j] = 0.0f;
			}
		}

		#pragma acc loop gang tile(1000,1000)
		for (i = 0; i < SIZE; ++i)
		{
			#pragma acc loop vector
			for (j = 0; j < SIZE; ++j)
			{
				c[i][j] = a[i][j] + b[i][j];
			}
		}
	}

	return 0;
}
