#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char *argv[]) /* acc_data.c */
{
	int n = 30000000;   /* tamanho dos vetores  */
	double soma = 0.0;

	/* Atribui os valores iniciais para os vetores de entrada */
	#pragma acc parallel loop gang reduction(+ : soma)
	for (int i = 0; i < n; i++)
		soma += sin(i) * sin(i) + cos(i) * cos(i);

	soma = soma / (double)n;

	printf("resultado final: %f\n", soma);
	
	return 0;
}
