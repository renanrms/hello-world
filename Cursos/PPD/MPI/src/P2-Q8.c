#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"

/* 
OBS.: Acredito que no enunciado era a quantidade de linahas que poderia ser maior que a de processos, porque a quantidade de colunas da matriz já estava assim no exemplo 3.22.
*/

void mxv(int m, int n, double *A, double *b, double *c);

int main(int argc, char *argv[])
{ /* mpi_mxv.c  */
	double *A=NULL, *Aloc, *b, *c;
	int i, j, m, n;
	int meu_ranque, num_procs, raiz = 0;
	double start, finish, loc_elapsed, elapsed;
	int scanf_return;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &meu_ranque);
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs);

	if (meu_ranque == 0)
	{
		printf("Por favor entre com n: \n");
		scanf_return = scanf("%d", &n);
		if (scanf_return == 0)
			return 1;
		printf("\n");
		printf("Por favor entre com m: \n");
		scanf_return = scanf("%d", &m);
		if (scanf_return == 0)
			return 1;
		printf("\n");
	}

	MPI_Bcast(&n, 1, MPI_INT, raiz, MPI_COMM_WORLD);
	MPI_Bcast(&m, 1, MPI_INT, raiz, MPI_COMM_WORLD);

	if (m % num_procs != 0)
	{
		if (meu_ranque == 0) 
		printf("Entre com uma quantidade de linhas múltipla da quantidade de processos.\n");
		MPI_Finalize();
		return 0;
	}
	MPI_Barrier(MPI_COMM_WORLD);

	int linhas_por_processo = m / num_procs;

	if (meu_ranque == 0)
	{
		printf("Valor de m: %d e  n: %d \n", m, n);
		A = (double *)malloc(m * n * sizeof(double));
		Aloc = (double *)malloc(m * n * sizeof(double));
		b = (double *)malloc(n * sizeof(double));
		c = (double *)malloc(m * sizeof(double));
	}
	else
	{
		Aloc = (double *)malloc(linhas_por_processo * n * sizeof(double));
		b = (double *)malloc(n * sizeof(double));
		c = (double *)malloc(linhas_por_processo * sizeof(double));
	}

	if (meu_ranque == 0)
	{
		printf("Atribuindo valor inicial à matriz A e ao vetor b\n");
		for (j = 0; j < n; j++)
			b[j] = 2.0;
		for (i = 0; i < m; i++)
			for (j = 0; j < n; j++)
				A[i * n + j] = (double)i;
	}
	/* Difunde o vetor b para todos os processos */
	MPI_Bcast(&b[0], n, MPI_DOUBLE, raiz, MPI_COMM_WORLD);

	/* Distribui as linhas da matriz A entre todos os processos */
	MPI_Scatter(A, linhas_por_processo * n, MPI_DOUBLE, Aloc, linhas_por_processo * n, MPI_DOUBLE, raiz, MPI_COMM_WORLD);

	start = MPI_Wtime();

	(void)mxv(linhas_por_processo, n, Aloc, b, c);

	MPI_Gather(c, linhas_por_processo, MPI_DOUBLE, c, linhas_por_processo, MPI_DOUBLE, raiz, MPI_COMM_WORLD);

	finish = MPI_Wtime();
	loc_elapsed = finish - start;
	MPI_Reduce(&loc_elapsed, &elapsed, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

	if (meu_ranque == 0)
	{
		printf("Tempo total = %e\n", elapsed);
	}
	free(A);
	free(Aloc);
	free(b);
	free(c);

	MPI_Finalize();
	return (0);
}

void mxv(int m, int n, double *A, double *b, double *c)
{
	int row, col;

	for (row = 0; row < m; row++)
	{
		c[row] = 0.0;

		for (col = 0; col < n; col++)
			c[row] += A[row * m + col] * b[col];
	}
}
