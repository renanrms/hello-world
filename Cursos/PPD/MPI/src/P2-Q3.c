#include <stdio.h>
#include <stdlib.h>

#include "mpi.h"

#define MAX 100

/* O programa finaliza normalmente a persar das comunicações ainda não terminadas. */

int main(int argc, char *argv[])
{ /* mpi_aleatorio.c  */
	int meu_ranque, total_num, etiq = 0;
	int numeros[MAX];
	MPI_Status estado;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &meu_ranque);

	if (meu_ranque != 0)
	{
		/* Escolhe uma quantidade aleatória de inteiros para enviar para o processo 0 */
		srand(MPI_Wtime() + meu_ranque);
		total_num = (rand() / (float)RAND_MAX) * MAX;

		/* Envia a quantidade de inteiros para o processo 0 */
		MPI_Send(numeros, total_num, MPI_INT, 0, etiq, MPI_COMM_WORLD);
		printf("Processo %d enviou %d números para 0\n", meu_ranque, total_num);
	}
	else
	{
		/* Recebe no máximo MAX números de algum processo */
		MPI_Recv(numeros, MAX, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &estado);

		/* Quando chega a mensagem, verifica o status para determinar quantos números foram realmente recebidos */
		MPI_Get_count(&estado, MPI_INT, &total_num);

		/* Imprime a quantidade de números e a informação adicional que está no manipulador "estado" */
		printf("Processo 0 recebeu %d números. Origem da mensagem = %d, etiqueta = %d\n", total_num, estado.MPI_SOURCE, estado.MPI_TAG);
	}

	MPI_Finalize();

	return (0);
}