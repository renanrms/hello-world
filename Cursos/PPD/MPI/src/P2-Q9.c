#include <stdio.h>
#include <string.h>

#include "mpi.h"

int main(int argc, char *argv[])
{ /* mpi_simples.c  */
	int meu_ranque, num_procs;
	int origem, dest, etiq = 0;
	int mensagem = 1;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &meu_ranque);
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs);

	dest = (meu_ranque + 1) % num_procs;
	origem = (meu_ranque + num_procs - 1) % num_procs;

	/* O processo 0 inicia a comunicação do anel */
	if (meu_ranque == 0)
	{
		mensagem = 10;
		MPI_Send(&mensagem, 1, MPI_INT, dest, etiq, MPI_COMM_WORLD);
	}

	while (mensagem != 0)
	{
		MPI_Recv(&mensagem, 1, MPI_INT, origem, etiq, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		printf("Processo %d:  mensagem recebida = %d\n", meu_ranque, mensagem);
		if (mensagem != 0)
			mensagem--;
		MPI_Send(&mensagem, 1, MPI_INT, dest, etiq, MPI_COMM_WORLD);
	}

	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
	return 0;
}