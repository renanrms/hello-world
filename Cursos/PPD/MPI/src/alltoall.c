#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"

#define N_ELEMENTOS 2

int main(int argc, char *argv[])
{
	int i, meu_ranque, num_procs;
	int *vetor_entrada, *vetor_saida;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &meu_ranque);
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs);

	/* Aloca e atribui valor inicial os vetores */
	vetor_entrada = (int *)malloc(num_procs * sizeof(int) * N_ELEMENTOS);
	vetor_saida = (int *)malloc(num_procs * sizeof(int) * N_ELEMENTOS);
	for (i = 0; i < num_procs * N_ELEMENTOS; i++)
		vetor_entrada[i] = meu_ranque * num_procs * N_ELEMENTOS + i;
		
	/* Imprime a entrada dada para a função MPI_Alltoall */
	printf("Entrada do processo %d: ", meu_ranque);
	for (i = 0; i < num_procs * N_ELEMENTOS; i++)
	{
		printf("%d ", vetor_entrada[i]);
	}
	printf("\n");

	/* Reorganiza os dados */
	MPI_Alltoall(vetor_entrada, N_ELEMENTOS, MPI_INT, vetor_saida, N_ELEMENTOS, MPI_INT, MPI_COMM_WORLD);
	
	/* Imprime a saida da função MPI_Alltoall */
	printf("Saida do processo %d: ", meu_ranque);
	for (i = 0; i < num_procs * N_ELEMENTOS; i++)
	{
		printf("%d ", vetor_saida[i]);
	}
	printf("\n");

	/* Desaloca os vetores e finaliza o MPI */
	free(vetor_saida);
	free(vetor_entrada);
	MPI_Finalize();

	return (0);
}
