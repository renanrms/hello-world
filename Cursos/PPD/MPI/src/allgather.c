#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "mpi.h"

#define MIN_NELEM 1024
#define MAX_NELEM 4096

int main(int argc, char *argv[])
{
	int i, meu_ranque, num_procs;
	float *medias_parc = NULL;
	float *sub_nums_aleat = NULL;
	float soma_local = 0, soma = 0;
	float media, media_local;
	int nelem_local;
	int *nelem;
	int total_elem = 0;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &meu_ranque);
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs);

	/* Alimenta o gerador de números aleatórios com valores diferentes para cada processo */
	srand(MPI_Wtime() * (meu_ranque + 1));

	/* Cria um vetor de números aleatórios em todos os processos. Cada número tem um valor entre 0 e 1 */
	nelem_local = MIN_NELEM + rand() % (MAX_NELEM - MIN_NELEM);
	sub_nums_aleat = (float *)malloc(sizeof(float) * nelem_local);
	for (i = 0; i < nelem_local; i++)
		sub_nums_aleat[i] = (rand() / (float)RAND_MAX);

	/* Soma os números localmente */
	for (i = 0; i < nelem_local; i++)
		soma_local += sub_nums_aleat[i];
	media_local = soma_local / nelem_local;

	/* Imprime a soma e média dos números aleatórios em cada processo */
	printf("Soma local para o processo %d = %f, tamanho do vetor = %d, media local = %f\n", meu_ranque, soma_local, nelem_local, media_local);

	/* Coleta todas as médias parciais de todos os processos */
	medias_parc = (float *)malloc(sizeof(float) * num_procs);
	MPI_Allgather(&media_local, 1, MPI_FLOAT, medias_parc, 1, MPI_FLOAT, MPI_COMM_WORLD);

	/* Coleta todas as quantidades de elementos de todos os processos */
	nelem = (int *)malloc(sizeof(int) * num_procs);
	MPI_Allgather(&nelem_local, 1, MPI_INT, nelem, 1, MPI_INT, MPI_COMM_WORLD);

	/* Calcula a média total. */
	for (i = 0; i < num_procs; i++)
	{
		soma += medias_parc[i] * nelem[i];
		total_elem += nelem[i];
	}
	media = soma / total_elem;
	printf("Média geral calculada no processo %d = %f, número total de elementos = %d\n", meu_ranque, media, total_elem);

	/* Libera espaço alocado  */
	free(nelem);
	free(medias_parc);
	free(sub_nums_aleat);
	MPI_Finalize();

	return (0);
}