#include <stdio.h>
#include <string.h>

#include "mpi.h"

int main(int argc, char *argv[])
{ /* mpi_simples.c  */
	int meu_ranque, num_procs;
	int origem, destino, etiq = 0;
	char mensagem[200];
	MPI_Status estado;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &meu_ranque);
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs);

	/* Todos os processos com ranque diferente de 0 enviam uma mensagem*/
	if (meu_ranque != 0)
	{
		sprintf(mensagem, "Processo %d está vivo!", meu_ranque);
		destino = 0;
		MPI_Send(mensagem, strlen(mensagem) + 1, MPI_CHAR, destino, etiq, MPI_COMM_WORLD);
	}
	else
	{
		/* Processo com ranque 0 recebe num_procs-1 mensagens */
		for (origem = 1; origem < num_procs; origem++)
		{
			/* Recebe as mensagens */
			
			//MPI_Recv(mensagem, 200, MPI_CHAR, origem, etiq, MPI_COMM_WORLD, &estado);
			MPI_Recv(mensagem, 200, MPI_CHAR, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &estado);
			
			/* Imprime as mensagens recebidas */
			/* Como o coringa foi usado para a source, não necessariamente as mensagens seguirão a ordem da variável origem, mas todas serão impressas. */
			
			printf("%s\n", mensagem);
		}
	}

	MPI_Finalize();
	
	return (0);
}