#include <stdio.h>
#include <stdlib.h>

#include "mpi.h"

#define TAM_MAX 65536

int main(int argc, char *argv[])
{
	int meu_ranque, num_procs;
	int sucessor, predecessor;
	float a;
	float vetor[TAM_MAX];
	MPI_Status status;
	MPI_Request request;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &meu_ranque);
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs);

	sucessor = (meu_ranque + 1) % num_procs;
	predecessor = (meu_ranque - 1 + num_procs) % num_procs;
	
	MPI_Isend(&a, 1, MPI_FLOAT, sucessor, 1, MPI_COMM_WORLD, &request);
	MPI_Isend(vetor, TAM_MAX, MPI_FLOAT, sucessor, 1, MPI_COMM_WORLD, &request);
	MPI_Recv(&a, 1, MPI_FLOAT, predecessor, 1, MPI_COMM_WORLD, &status);
	MPI_Recv(vetor, TAM_MAX, MPI_FLOAT, predecessor, 1, MPI_COMM_WORLD, &status);

	MPI_Wait(&request, &status);

	printf("Processo %d: Dados recebidos do predecessor %d e enviados para o sucessor %d.\n", meu_ranque, predecessor, sucessor);

	MPI_Finalize();

	return 0;
}