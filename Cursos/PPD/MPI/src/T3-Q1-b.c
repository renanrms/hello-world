#include <stdio.h>
#include <stdlib.h>

#include "mpi.h"

#define TAM_MAX 65536

int main(int argc, char *argv[])
{
	int meu_ranque, num_procs;
	int sucessor, predecessor;
	float a;
	float vetor[TAM_MAX];
	void *buffer;
	int tam_buffer;
	MPI_Status status;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &meu_ranque);
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs);

	sucessor = (meu_ranque + 1) % num_procs;
	predecessor = (meu_ranque - 1 + num_procs) % num_procs;

	MPI_Pack_size(TAM_MAX + 1, MPI_FLOAT, MPI_COMM_WORLD, &tam_buffer);
	tam_buffer += 2 * MPI_BSEND_OVERHEAD;
	buffer = (void *)malloc(tam_buffer);
	MPI_Buffer_attach(buffer, tam_buffer);

	MPI_Bsend(&a, 1, MPI_FLOAT, sucessor, 1, MPI_COMM_WORLD);
	MPI_Bsend(vetor, TAM_MAX, MPI_FLOAT, sucessor, 1, MPI_COMM_WORLD);
	MPI_Recv(&a, 1, MPI_FLOAT, predecessor, 1, MPI_COMM_WORLD, &status);
	MPI_Recv(vetor, TAM_MAX, MPI_FLOAT, predecessor, 1, MPI_COMM_WORLD, &status);

	printf("Processo %d: Vetor recebido do predecessor %d e enviado para o sucessor %d.\n", meu_ranque, predecessor, sucessor);

	MPI_Buffer_detach(&buffer, &tam_buffer);
	free(buffer);

	MPI_Finalize();

	return 0;
}