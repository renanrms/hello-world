#include <stdio.h>
#include <stdlib.h>

#include "mpi.h"

void mxv(int m, int n, double *A, double *b, double *c);
void tranpose(int m, int n, const double *A, double *A_t);
void print_matrix(int m, int n, double *A);

int main(int argc, char *argv[])
{
	double *A = NULL, *A_t = NULL, *b = NULL, *c = NULL;
	double *A_local = NULL, *A_t_local = NULL, *b_local = NULL, *c_local = NULL;
	int i, j;
	int m, n, n_local;
	int meu_ranque, num_procs;
	double tempo_inicial = 0.0, tempo_final=0.0;
	int error;
	FILE* resultados;

	/* Inicializa o MPI */
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &meu_ranque);
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs);

	if (meu_ranque == 0 && argc != 3)
	{
		printf("\nNúmero incorreto de parâmetros.\n");
		printf("Uso:\n\t%s m n\n\tm é o número de linhas.\n\tn é o número de colunas.\n", argv[0]);
		exit(1);
	}

	/* Barreira para evitar que processos sigam antes do tratamento de erro ser realizado pelo processo raiz */
	MPI_Barrier(MPI_COMM_WORLD);

	m = atoi(argv[1]);
	n = atoi(argv[2]);

	if (meu_ranque == 0)
	{
		if (n % num_procs != 0)
		{
			printf("O número de colunas precisa ser múltiplo da quantidade de processos.\n");
			exit(1);
		}

		/* Alocando vetores no processo raiz para armazenar os dados completos */
		printf("Valor de m: %d e  n: %d \n", m, n);
		A = (double *)malloc(m * n * sizeof(double));
		A_t = (double *)malloc(m * n * sizeof(double));
		b = (double *)malloc(n * sizeof(double));
		c = (double *)malloc(m * sizeof(double));

		/* Inicializando a matriz e o vetor */
		for (j = 0; j < n; j++)
			b[j] = 2.0;
		for (i = 0; i < m; i++)
			for (j = 0; j < n; j++)
				A[i * n + j] = (double)i;

		/* Exibe as matrizes */
		//printf("\nMatriz A =\n");
		//print_matrix(m, n, A);

		//printf("\nVetor b =\n");
		//print_matrix(m, 1, b);

		/* Contabiliza o tempo inicial */
		tempo_inicial = MPI_Wtime();

		tranpose(m, n, A, A_t);
	}

	/* Alocando vetores para os dados locais */
	n_local = n / num_procs;
	A_local = (double *)malloc(m * n_local * sizeof(double));
	A_t_local = (double *)malloc(m * n_local * sizeof(double));
	b_local = (double *)malloc(n_local * sizeof(double));
	c_local = (double *)malloc(m * sizeof(double));

	/* Distribuindo os vetores parciais para os processos */
	MPI_Scatter(A_t, m*n_local, MPI_DOUBLE, A_t_local, m*n_local, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	tranpose(n_local, m, A_t_local, A_local);
	MPI_Scatter(b, n_local, MPI_DOUBLE, b_local, n_local, MPI_DOUBLE, 0, MPI_COMM_WORLD);

	/* Multiplicando a matriz e o vetor locais em cada processo */
	mxv(m, n_local, A_local, b_local, c_local);

	/* Reduzindo os resultados de cada processo com uma soma recebida do processo raiz */
	MPI_Reduce(c_local, c, m, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

	/* Exibição do desempenho */
	if (meu_ranque == 0)
	{
		tempo_final = MPI_Wtime();

		//printf("\nResultado: vetor c =\n");
		//print_matrix(m, 1, c);

		printf("\nForam gastos %3.6f segundos com dimensões %dx%d.\n", tempo_final - tempo_inicial, m, n);

		if (!fopen("./evaluation/mxv.csv", "r"))
		{
			error = system("echo \"n_processes,wtime\" > ./evaluation/mxv.csv");
			if (error) {
				exit(1);
			}
		}

		resultados = fopen("./evaluation/mxv.csv", "a");
		fprintf(resultados, "%i,%f\n", num_procs, tempo_final - tempo_inicial);
		fclose(resultados);
	}

	/* Libera memória e finaliza o MPI */
	free(c_local);
	free(b_local);
	free(A_t_local);
	free(A_local);
	if (meu_ranque == 0)
	{
		free(c);
		free(b);
		free(A_t);
		free(A);
	}

	MPI_Finalize();

	return 0;
}

void mxv(int m, int n, double *A, double *b, double *c)
{
	int i, j;

	for (i = 0; i < m; i++)
	{
		c[i] = 0.0;
		for (j = 0; j < n; j++)
			c[i] += A[i * n + j] * b[j];
	}
}

void tranpose(int m, int n, const double *A, double *A_t)
{
	int i, j;

	for (i = 0; i < m; i++)
	{
		for (j = 0; j < n; j++)
		{
			/* Para A[mxn]   -> i é a linha e j é a coluna */
			/* Para A_t[nxm] -> i é a coluna e j é a linha */

			A_t[j * m + i] = A[i * n + j];
		}
	}
}

void print_matrix(int m, int n, double *A)
{
	int i, j;

	for (i = 0; i < m; i++)
	{
		for (j = 0; j < n; j++)
			printf("%f\t", A[i * n + j]);
		printf("\n");
	}
}