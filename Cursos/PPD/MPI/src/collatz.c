#include <stdio.h>
#include <stdlib.h>

#include "mpi.h"

unsigned long long collatz(unsigned long long N)
{
	unsigned long long steps = 0;

	while (N > 1)
	{
		N = (N%2 == 0) ? N/2 : 3*N + 1; /* sucessor */
		steps++;
	}

	return steps;
}

int main(int argc, char *argv[])
{
	int meu_ranque, num_procs;
	int dest, tag = 1, stop = 0;
	double tempo_inicial = 0.0, tempo_final;
	unsigned long long inicio;
	unsigned long long N_max;
	unsigned long long tamanho;
	unsigned long long i, numSteps, max_numSteps = 0;
	int error;
	MPI_Status status;
	FILE *resultados;

	/* Inicia o MPI e determina o ranque e o número de processos ativos */
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &meu_ranque);
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs);

	/* Tratamento de erro e barreira para evitar que qualquer processo siga sem terminar o tratamento. */
	if (meu_ranque == 0 && argc != 2)
	{
		printf("\nNúmero incorreto de parâmetros.\n");
		printf("Uso:\n\t%s n\n\tn é o maior número a testar para a conjectura de Collatz.\n", argv[0]);
		exit(1);
	}
	if (meu_ranque == 0 && num_procs < 2)
	{
		printf("\nDeve haver pelo menos dois processos para o funcionamento do programa.\n");
		exit(1);
	}
	MPI_Barrier(MPI_COMM_WORLD);

	N_max = atol(argv[1]);
	tamanho = (N_max/num_procs) / 1000; /* Garante boa distribuição de carga */
	if (tamanho < 100) /* Garante que tamanho não seja nulo e tenha um tamanho mínimo */
	{
		tamanho = 100;
	}

	if (meu_ranque == 0)
	{
		/* Processo principal */

		tempo_inicial = MPI_Wtime();

		for (dest = 1, inicio = 1; dest < num_procs; dest++, inicio += tamanho)
		{
			if (inicio > N_max)
			{
				/* Caso o final do intervalo tenha sido ultrapassado, deve ser enviada a tag 99 para que os processos secundários sejam finalizados */
				tag = 99;
				stop++;
			}
			MPI_Send(&inicio, 1, MPI_UNSIGNED_LONG_LONG, dest, tag, MPI_COMM_WORLD);
		}

		/* Fica recebendo as contagens parciais dos outros processos até todos terem terminado */
		while (stop < (num_procs - 1))
		{
			MPI_Recv(&numSteps, 1, MPI_UNSIGNED_LONG_LONG, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			if (numSteps > max_numSteps)
			{
				max_numSteps = numSteps;
			}
			dest = status.MPI_SOURCE;

			if (inicio > N_max)
			{
				tag = 99;
				stop++;
			}

			/* Envia um novo pedaço com tamanho números para o mesmo processo*/
			MPI_Send(&inicio, 1, MPI_UNSIGNED_LONG_LONG, dest, tag, MPI_COMM_WORLD);
			inicio += tamanho;
		}
	}
	else
	{
		/* Processo secundário */

		while (status.MPI_TAG != 99)
		{
			MPI_Recv(&inicio, 1, MPI_UNSIGNED_LONG_LONG, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			if (status.MPI_TAG != 99)
			{
				max_numSteps = 0;
				for (i = inicio; i < (inicio + tamanho) && i <= N_max; i++)
				{
					numSteps = collatz(i);
					if (numSteps > max_numSteps)
					{
						max_numSteps = numSteps;
					}
				}

				/* Envia a contagem parcial para o processo principal */
				MPI_Send(&max_numSteps, 1, MPI_UNSIGNED_LONG_LONG, 0, tag, MPI_COMM_WORLD);
			}
		}
	}

	/* Computa o tempo inicial no processo 0 e imprime os resultados */
	if (meu_ranque == 0)
	{
		tempo_final = MPI_Wtime();

		printf("Verificando até o número %llu, com %d processos.\nWtime: %f\nMaior número de passos: %llu\n", N_max, num_procs, tempo_final - tempo_inicial, max_numSteps);

		if (!fopen("./evaluation/collatz.csv", "r"))
		{
			error = system("echo \"n_processes,N_max,tamanho,wtime\" > ./evaluation/collatz.csv");
			if (error)
			{
				exit(1);
			}
		}

		resultados = fopen("./evaluation/collatz.csv", "a");
		fprintf(resultados, "%i,%llu,%llu,%f\n", num_procs, N_max, tamanho, tempo_final - tempo_inicial);
		fclose(resultados);
	}

	MPI_Finalize();

	return (0);
}