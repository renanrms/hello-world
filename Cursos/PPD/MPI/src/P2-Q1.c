#include <stdio.h>
#include <stdlib.h>

#include "mpi.h"

int main(int argc, char *argv[])
{
	int aux1, aux2, aux4, aux5, aux6;
	char aux3[MPI_MAX_PROCESSOR_NAME+1];
	MPI_Comm aux = MPI_COMM_WORLD;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(aux, &aux1);
	MPI_Comm_rank(aux, &aux2);
	MPI_Get_processor_name(aux3, &aux4);
	MPI_Get_version(&aux5, &aux6);

	/* Imprime os dados apenas no processo raiz */
	if (aux2 == 0)
	{
		printf("Número de processos: %d\n", aux1);
		printf("Meu número: %d\n", aux2);
		printf("Nome do processador: %s (Comprimento = %d)\n", aux3, aux4);
		printf("Versão: %d.%d\n", aux5, aux6);
	}

	MPI_Finalize();

	return 0;
}