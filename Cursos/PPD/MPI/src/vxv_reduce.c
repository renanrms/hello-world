#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "mpi.h"

void initializeVector(double *pVector, unsigned long n)
{
	int i;

	srand((unsigned)time(NULL));

	for (i = 0; i < n; i++)
	{
		pVector[i] = ((double)(rand() % 1000000)) / 1000000;
	}
}

int main(int argc, char *argv[])
{
	int meu_ranque, num_procs;				 /* respectivamente q e p */
	double tempo_inicial = 0.0, tempo_final; /* Tempo de execução */
	long int n = 1000000;					 /* Tamanho dos vetores */
	long int i;								 /* Índice de iteração */
	double *v1, *v2;						 /* vetores */
	double produto = 0.0, total;			 /* Produto vetorial de cada processo e total */
	int error;
	FILE *resultados;

	v1 = (double *)malloc(n * sizeof(double));
	v2 = (double *)malloc(n * sizeof(double));

	initializeVector(v1, n);
	initializeVector(v2, n);

	/* Inicia o MPI e determina o ranque e o número de processos ativos*/
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &meu_ranque);
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs);

	if (meu_ranque == 0)
	{
		tempo_inicial = MPI_Wtime();
	}

	/* Cada processo calcula o produto em n/num_procs posições dos vetores */
	for (i = meu_ranque; i < n; i += num_procs)
	{
		produto += v1[i] * v2[i];
	}

	/* O processo 0 soma os produtos parciais */
	MPI_Reduce(&produto, &total, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

	/* Imprime o resultado */
	if (meu_ranque == 0)
	{
		tempo_final = MPI_Wtime();
		printf("Foram gastos %3.6f segundos\n", tempo_final - tempo_inicial);
		printf("Com dois vetores de comprimento %ld\n", n);
		printf("Produto dos vetores = %lf \n", total);

		if (!fopen("./evaluation/produtoVetorial_reduce.csv", "r"))
		{
			error = system("echo \"n_processes,wtime\" > ./evaluation/produtoVetorial_reduce.csv");
			if (error)
			{
				exit(1);
			}
		}

		resultados = fopen("./evaluation/produtoVetorial_reduce.csv", "a");
		fprintf(resultados, "%i,%f\n", num_procs, tempo_final - tempo_inicial);
		fclose(resultados);
	}

	MPI_Finalize();

	return (0);
}