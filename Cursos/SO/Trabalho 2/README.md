# Disparador

Implementação de solução do problema "Room Party". A descrição do problema pode ser encontrada na seção 7.3 do livro:

https://greenteapress.com/semaphores/LittleBookOfSemaphores.pdf

O programa aceita, como parâmetro de linha de comando, o número de estudantes e há também alguns ajustes de parâmetros que podem ser feitos em macros no código fonte. Para os ajustes que foram feitos, uma recomendação inicial pode ser rodar o programa com 51 a 100 estudantes, para conseguir ver bem todas as etapas acontecendo.

Além das informações mostradas na tela, um arquivo `room_party.log` será gerado com todo o log do programa, mostrando cada ação de entrar ou sair da sala.

## Compilação

Para compilar o programa, basta usar o comando `make` na pasta do projeto. O executável será gerado na pasta bin.
