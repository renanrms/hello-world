/*
 * Implementação de solução do problema "Room Party".
 * 
 * A descrição do problema pode ser encontrada na seção 7.3 do livro:
 * https://greenteapress.com/semaphores/LittleBookOfSemaphores.pdf
 */

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/* Critério de parada */
#define MAX_TIMES_DEAN_EXIT_ROOM 5

/* Outros parâmetros do programa */
#define NUM_STUDENTS_FOR_A_PARTY 50
#define LOG_FILE_NAME            "room_party.log"

/* Ajustes de tempo. Alterar estes valores pode mudar a probabilidade de certos eventos acontecerem */
#define BASE_TIME                1000
#define SEARCH_DURATION          1000 * BASE_TIME
#define STUDENT_WAIT_MAX_TIME    2000 * BASE_TIME
#define DEAN_WAIT_MAX_TIME       50 * BASE_TIME
#define DISPLAY_INTERVAL         200 * BASE_TIME

typedef enum
{
	noAction = 0,
	conductingASearch,
	breakingAParty
} deanStateType;

typedef enum
{
	outsideTheRoom = 0,
	insideTheRoom
} studentStateType;

pthread_mutex_t roomDoor = PTHREAD_MUTEX_INITIALIZER; /* Representa a porta da sala. Apenas uma thread entra ou sai por vez. */
pthread_cond_t emptyRoom = PTHREAD_COND_INITIALIZER;  /* Sinal enviado para o Diretor quando a sala é esvaziada */
unsigned studentsInsideRoom = 0;
deanStateType deanState = noAction;
unsigned timesDeanExitedRoom = 0;
FILE *verboseOutput;

void *studentThread()
{
	studentStateType state = outsideTheRoom;
	while (timesDeanExitedRoom < MAX_TIMES_DEAN_EXIT_ROOM)
	{
		usleep(rand() % STUDENT_WAIT_MAX_TIME);

		/* Adquire o uso exclusivo da "porta" para entrar na região crítica */
		pthread_mutex_lock(&roomDoor);

		/* Libera o lock e aborta as ações se não estiver mais nas condições do programa */
		if (timesDeanExitedRoom >= MAX_TIMES_DEAN_EXIT_ROOM)
		{
			pthread_mutex_unlock(&roomDoor);
			continue;
		}

		if (state == outsideTheRoom)
		{
			if (deanState == noAction)
			{
				fprintf(verboseOutput, "Estudante entra na sala.\n");
				state = insideTheRoom;
				studentsInsideRoom++;
			}
		}
		else
		{
			if (deanState == breakingAParty)
			{
				fprintf(verboseOutput, "Estudante sai da sala.\n");
				state = outsideTheRoom;
				studentsInsideRoom--;
				if (studentsInsideRoom == 0)
				{
					pthread_cond_signal(&emptyRoom);
				}
			}
		}

		pthread_mutex_unlock(&roomDoor);
	}

	return NULL;
}

void *deanThread()
{
	while (timesDeanExitedRoom < MAX_TIMES_DEAN_EXIT_ROOM)
	{
		usleep(rand() % DEAN_WAIT_MAX_TIME);

		/* Adquire o uso exclusivo da "porta" para entrar na região crítica */
		pthread_mutex_lock(&roomDoor);

		if (studentsInsideRoom == 0)
		{
			fprintf(verboseOutput, "Diretor conduz uma busca.\n");
			deanState = conductingASearch;

			/* Libera a trava enquanto conduz a busca para que os estudantes não fiquem enfileirados esperando a liberação do lock. */
			/* Assim a sala não ficará cheia tão repentinamente. */
			pthread_mutex_unlock(&roomDoor);
			usleep(SEARCH_DURATION);
			pthread_mutex_lock(&roomDoor);

			fprintf(verboseOutput, "Diretor termina a busca.\n");
			deanState = noAction;
			timesDeanExitedRoom++;
		}
		else if (studentsInsideRoom > NUM_STUDENTS_FOR_A_PARTY)
		{
			fprintf(verboseOutput, "Diretor para a festa.\n");
			deanState = breakingAParty;
			pthread_cond_wait(&emptyRoom, &roomDoor);
			fprintf(verboseOutput, "Festa terminada.\n");
			deanState = noAction;
			timesDeanExitedRoom++;
		}

		pthread_mutex_unlock(&roomDoor);
	}

	return NULL;
}

int main(int argc, char **argv)
{
	unsigned nStudents, i;
	pthread_t *students;
	pthread_t dean;

	/* Leitura dos argumentos */

	if (argc != 2)
	{
		printf("Esperava-se 1 argumentos, mas %d foram passados.\n", argc - 1);
		printf("\nUso: %s N\n\n", argv[0]);
		printf("\tN\té o número de estudantes que haverá no total.\n");
		return 1;
	}

	nStudents = (unsigned)atoi(argv[1]);
	if (nStudents <= NUM_STUDENTS_FOR_A_PARTY)
	{
		printf("O número de estudantes deve ser maior que %u para evitar deadlocks.\n", NUM_STUDENTS_FOR_A_PARTY);
		return 1;
	}

	srand((unsigned)time(NULL));
	verboseOutput = fopen(LOG_FILE_NAME, "w");

	/* Cria as threads dos estudantes e do diretor */

	printf("Criando threads para %u estudantes e 1 diretor.\n\n", nStudents);
	students = (pthread_t *)malloc(nStudents * sizeof(pthread_t));
	for (i = 0; i < nStudents; i++)
	{
		pthread_create(&students[i], NULL, studentThread, NULL);
	}
	pthread_create(&dean, NULL, deanThread, NULL);

	/* Imprime o estado atual das variáveis enquanto durar a sessão */

	while (timesDeanExitedRoom < MAX_TIMES_DEAN_EXIT_ROOM)
	{
		usleep(DISPLAY_INTERVAL);

		/* Obtém a trava para garantir que os dados mostrados serão coerentes */

		pthread_mutex_lock(&roomDoor);
		printf("Estudantes na sala: %u\tDiretor na sala? ", studentsInsideRoom);
		if (deanState == conductingASearch)
			printf("Sim (conduzindo uma busca)");
		else if (deanState == breakingAParty)
			printf("Sim (acabando com uma festa)");
		else
			printf("Não");
		printf("\n");
		pthread_mutex_unlock(&roomDoor);
	}

	/* Aguarda a finalização das threads e termina o programa */

	printf("Finalizando o programa...\n");
	for (i = 0; i < nStudents; i++)
	{
		pthread_join(students[i], NULL);
	}
	pthread_join(dean, NULL);

	fclose(verboseOutput);

	return 0;
}