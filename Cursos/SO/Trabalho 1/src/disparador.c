/*
 * Código desenvolvidos para sistemas Linux.
 * Testado em:
 * 	- Linux Mint 19.2
 */

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

#define COMMAND_MAX_LENGTH   2048
#define EOS                  (char)0

typedef enum 
{
	noError = 0, 
	commandReadingError, 
	emptyComand, 
	signalHandlingSetupFail, 
	forkFail, 
	execFail
} 
errorType;

/* 
 * Abaixo são definidas algumas variáveis globais para comunicar os dados da
 * função main com os handlers dos sinais.
 *
 * Para os vetores que armazenam o argv dos processos que serão chamados, a
 * quantidade de argumentos é no máximo a metade + 1 do tamanho do comando
 * porém deve haver 1 elemento a mais com valor NULL no final do array,
 * portanto, o tamanho máximo corresponde a COMMAND_MAX_LENGTH/2 + 2.
 */
char *p1_argv[COMMAND_MAX_LENGTH/2 + 2];
char *p2_argv[COMMAND_MAX_LENGTH/2 + 2];
int signalReceived = 0;
errorType handlerError = noError;

void handler_SIGTERM(int signo);
void handler_SIGUSR(int signo);
errorType receiveCommandFromUser(char *command, char **argv);

int main(int argc, char **argv)
{
	errorType error;
	// Soma-se 2 ao tamanho do comando devido ao '\n' e ao EOS que serão incluídos quando o usuário digitar um comando.
	char p1_command[COMMAND_MAX_LENGTH + 2];
	char p2_command[COMMAND_MAX_LENGTH + 2];
		
	printf("Este programa executará comandos de acordo com os sinais por ele recebidos.\n");

	// Recebe os comandos do usuário para executar futuramente.

	printf("\nDigite o primeiro comando: ");
	error = receiveCommandFromUser(p1_command, p1_argv);
	if (error != noError)
	{
		return error;
	}

	printf("\nDigite o segundo comando: ");
	error = receiveCommandFromUser(p2_command, p2_argv);
	if (error != noError)
	{
		return error;
	}

	// Configura as rotinas de tratamento de sinais.
	if (signal(SIGTERM, handler_SIGTERM) == SIG_ERR ||
		signal(SIGUSR1, handler_SIGUSR) == SIG_ERR  ||
		signal(SIGUSR2, handler_SIGUSR) == SIG_ERR
		)
	{
		printf("\nFalha ao configurar o tratamento dos sinais da aplicação.\n");
		printf("Abortando o programa...\n");
		return signalHandlingSetupFail;
	}

	// Espera para manter o processo em execução sem uso intensivo de CPU.
	while (signalReceived != SIGTERM)
	{
		if (handlerError != noError)
			return handlerError;

		sleep(1);
	}

	return noError;
}

errorType receiveCommandFromUser(char *command, char **argv)
{
	unsigned index = 0;

	// Obtém o comando pela entrada do usuário.
	if (fgets(command, COMMAND_MAX_LENGTH + 2, stdin) == NULL)
	{
		printf("\nErro de leitura do comando. OBS: o comando digitado não pode exceder %d caracteres.\n", COMMAND_MAX_LENGTH);
		return commandReadingError;
	}

	// Elimina o '\n' ao final do comando, sobrescrevendo com um EOS.
	command[strlen(command) - 1] = EOS;

	// Constroi o argv separando o comando pelos espaços.
	argv[index] = strtok(command, " "); 
	while (argv[index] != NULL)
	{
		index++;
		argv[index] = strtok(NULL, " ");
	}

	// Verifica se houve conteúdo no comando.
	if (argv[0] == NULL ||
		argv[0][0] == EOS
		)
	{
		printf("\nO comando passado está vazio.\n");
		return emptyComand;
	}

	return noError;
}

void handler_SIGTERM(int signo)
{
	printf("\nFinalizando o disparador...\n");
	signalReceived = SIGTERM;
}

void handler_SIGUSR(int signo)
{
	pid_t pid;
	char **argv;
	unsigned index;

	printf("\n");

	pid = fork();

	if (pid == -1)
	{
		// Falha no fork.

		handlerError = forkFail;
		printf("Não foi possível criar o processo filho.\n");
	}
	else if (pid == 0)
	{
		// Processo criado: este é o processo filho.

		// Seleciona o comando correto a executar de acordo com o sinal.
		if (signo == SIGUSR1)
		{
			// Sinal SIGUSR1 -> comando 1
			argv = p1_argv;
		}
		else
		{
			// Sinal SIGUSR2 -> comando 2
			argv = p2_argv;
		}
		
		// Executa o comando através do seu argv. Em caso de retorno -1, houve um erro e o processo filho será terminado na função main com retorno não nulo.
		if (execvp(argv[0], argv) == -1)
		{
			handlerError = execFail;
			printf("Não foi possível executar o comando:");

			for (index = 0; argv[index] != NULL; index++)
			{
				printf(" %s", argv[index]);
			}
			printf("\n");
		}
	}
	else
	{
		// Processo criado: este é o processo pai.

		// O processo pai aguardará o término do filho, e não haverá tratamento no pai de possíveis erros no filho como a não execução do comando.
		wait(NULL);
	}
}
