# Disparador

Este programa roda comandos passados pelo usuário em processos filhos de acordo com sinais recebidos:

* Para rodar o primeiro comando, envie o sinal SIGUSR1 ao processo;
* Para rodas o segundo comando, envie o sinal SIGUSR2 ao processo;
* Para finalizar o processo, envie o sinal SIGTERM;

A passagem dos comandos para o programa deve ser feita com o comando inteiro na mesma linha. A quebra de linha marcará o fim da digitação de um comando.

## Compilação

Para compilar o programa, basta usar o comando make na pasta do projeto. O executável será gerado na pasta bin.
