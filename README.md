# hello-world

Repositório utilizado para aprender sobre Git e programação e compartilhar alguns códigos úteis.

## Principais projetos neste repositório

* **Barra de Progresso**: é uma barra de progresso para uma campanha de arrecadação, feita com HTML, CSS e JavaScript. Ela cresce do zero até o percentual indicado assim que a página é renderizada e exibe o percentual dentro da região preenchida (acima de um certo valor). Todos os valores que definem o estilo da barrinha form bem organizados como variáveis em CSS para facilitar a estilização a gosto de cada um.
* **Eliminação de Gauss**: É um programa em Python que faz eliminação de Gauss na matriz ampliada de um sistema e imprime cada etapa detalhadamente mostrando as modificações feitas, com a matriz escrita em LaTeX para fácil incorporação em um documento.
* **Combinação de Resistores - (C++/Java)**: É um programa pensado para cálculo de divisores de tensão com múltiplos resistores. Quando se precisa em um circuito, gerar várias tensões indermediárias a partir de uma fonte de tensão, a escolha dos melhores valores comerciais possíveis para os resistores pode ser crucial otimizar a precisão dessas tensões geradas e a resistência total deste resistor. Para isso o programa mostra as melhores combinações encontradas em uma tabela, junto com algumas métricas para o usuário avaliar a presição das tensões geradas e a resistência total do divisor. *Implementei primeiramente em Java e depois em C++ pra aprender e não lembro qual está melhor, mas lembro que a Java funciona.*
* **Biblioteca RNA MLP**: Biblioteca em Python para contruir Redes Neurais Artificiais do tipo Multi-Layer Perceptron, com calculos feitos através da biblioteca numpy.
* **Biblioteca RNA Flexível**: Tentativa de fazer uma biblioteca em Python para construção de redes neurais com um funcionamento bem flexível, que permitiria criar redes com diferentes arquiteturas usando uma lógica bem rígida de orientação a objetos. A ideia era partir de uma classe Neurônio e contruir a rede com os neurônios. A ideia esbarrou em algumas dificuldades de implementação, principalmente na implementação do backpropagation, então está incompleto mas talvez eu volte a isso um dia.

**Renan Passos**, autor deste repositório e deste *readme.md* estuda **Engenharia Eletrônica e de Computação** na Universidade Federal do Rio de Janeiro e tem interesse por aprender mais sobre Linux, projetos open-source em geral e linguagens de programação.
