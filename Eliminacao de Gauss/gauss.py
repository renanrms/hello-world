def multiplica_linha(L, const):
    L = L.copy()
    for i in range(len(L)):
        L[i] *= const
    return L

def soma_linha(L1, L2):
    for i in range(len(L1)):
        L1[i] += L2[i]
    return L1

def reduz_linha(L, L_ref, Ci):
    const = - L[Ci] / L_ref[Ci]
    return soma_linha(L, multiplica_linha(L_ref, const))

def swap_linha(M, i, j):
    (M[i], M[j]) = (M[j], M[i])
    return M

def printa_bonito(M, latex=False):
    if (latex == False):
        for i in range(len(M)):
            print(M[i])
    else:
        print(r'\[')
        print(r'\left[\begin{array}{' + 'c'*(len(M[0]) - 1) + r'|c}')
        
        for i in range(len(M)):
            for j in range(len(M[0])):
                print(M[i][j], end='')
                if (j < len(M[0]) - 1):
                    print (r' & ', end='')
            if (i < len(M) - 1):
                print(r' \\ ')
        
        print()
        
        print(r'\end{array}\right]')
        print(r'\]')
    print()
  
def metodo_gauss(M, Li=0, Ci=0):
    printa_bonito(M, True)
    
    eps = 0.000001
        
    if ( (Li == len(M) - 1) or (Ci == len(M[0]) - 2) ):
        return M
    
    # Garante que tenha número diferente de zero na posição inicial
    if (-eps <= M[Li][Ci] <= eps):
        for i in range(Li+1, len(M)):
            if (M[i][Ci] != 0):
                M = swap_linha(M, Li, i)
                print(f'Agora tivemos que trocar a ordem da linha {Li} com a {i} para evitar um zero na diagonal principal.\n')
                printa_bonito(M, True)
                break
    
    if (-eps <= M[Li][Ci] <= eps):
        print('O sistema é indeterminado ou possui um determinante próximo de zero.')
        exit()
    
    print(f'Agora vamos eliminar os coeficientes abaixo da diagonal principal na coluna {Ci}.\n')
    for i in range(Li+1, len(M)):
        M[i] = reduz_linha(M[i], M[Li], Ci)
    
    metodo_gauss(M, Li+1, Ci+1)
    
    return M
