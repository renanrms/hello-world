#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Este programa usa o método de eleminação de Gauss para simplificar um sistema linear e imprime a matriz em cada passo do processo, formatada com LaTeX para que o usuário possa incluir em um documento desse tipo. 

import gauss

# Matriz ampliada do sistema linear.
A_amp = [
    [ 7 ,  0, -1,  -2,  0],
    [ 35,  0, -9,  -4, -7],
    [ 91,  6,-18, -21, -4],
    [ 42,  6, -7, -13,  4]
]

A_amp = gauss.metodo_gauss(A_amp)