#include "e_series.h"
#include "resistor.h"

#include <cmath>
#include <vector>

Resistor::Resistor (E_Series initialSerie) {
  setSerie (initialSerie);
}

void Resistor::setSerie (E_Series newSerie) {
  if      (newSerie == E3  ) { serieValues = E_SeriesBaseValues.E3;   }
  else if (newSerie == E6  ) { serieValues = E_SeriesBaseValues.E6;   }
  else if (newSerie == E12 ) { serieValues = E_SeriesBaseValues.E12;  }
  else if (newSerie == E24 ) { serieValues = E_SeriesBaseValues.E24;  }
  else if (newSerie == E48 ) { serieValues = E_SeriesBaseValues.E48;  }
  else if (newSerie == E96 ) { serieValues = E_SeriesBaseValues.E96;  }
  else if (newSerie == E192) { serieValues = E_SeriesBaseValues.E192; }

  serie = newSerie;
}

E_Series Resistor::getSerie () const {
  return serie;
}

std::vector<double> Resistor::getSerieValues () const {
  return serieValues;
}

double Resistor::getValue (int index) const {
  int neg = (index%serie < 0) ? 1 : 0; // 1 se o resto for negativo e 0 caso contrário.
  return getValue (index % serie + serie*neg, index/serie - neg);
}

double Resistor::getValue (int index, int exponent) const {
  return serieValues.at(index) * pow(10, exponent);
}

std::vector<double> Resistor::getRangeValues (double min, double max) const {
  int index = 0;
  std::vector<double> rangeValues = std::vector<double>();

  while (getValue(index) >= min) {
    index--;
  }

  index++;

  while (getValue(index) <= max) {
    rangeValues.push_back(getValue(index));
    index++;
  }

  return rangeValues;
}
