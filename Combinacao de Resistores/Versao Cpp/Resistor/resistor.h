#ifndef RESISTOR
#define RESISTOR "resistor.h"

#include "e_series.h"

#include <vector>


class Resistor {
  public:
    Resistor (E_Series initialSerie = E12);
    void setSerie (E_Series newSerie);
    E_Series getSerie () const;
    double getValue (int index) const;
    double getValue (int index, int exponent) const;
    std::vector<double> getSerieValues () const;
    std::vector<double> getRangeValues (double min, double max) const;

  private:
    E_Series serie;
    std::vector<double> serieValues;

};

#endif
