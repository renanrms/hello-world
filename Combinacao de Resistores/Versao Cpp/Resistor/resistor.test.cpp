#include "resistor.h"

#include <iostream>
#include <cmath>

using namespace std;


void PrintValues (int begin, int end, E_Series aSerie);

int main (int argc, char** argv) {

  PrintValues (-E3, 2*E3 - 1, E3);
  PrintValues (-E6, 2*E6 - 1, E6);
  PrintValues (-E12, 2*E12 - 1, E12);
  PrintValues (0, E24 - 1, E24);
  PrintValues (0, E48 - 1, E48);
  PrintValues (0, E96 - 1, E96);
  PrintValues (0, E192 - 1, E192);

  return 0;
}

void PrintValues (int begin, int end, E_Series aSerie) {
  int index = begin;
  Resistor resistor(aSerie);

  while (index <= end) {
    cout << resistor.getValue(index) << "  ";
    index++;
  }

  cout << endl;
}
