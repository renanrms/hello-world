#include "Resistor/resistor.h"
//#include "Resistor/e_series.h"

#include <cmath>
#include <iomanip>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

// -------- Inputs --------
// Objeto do resistor genérico na série correta.
// No argumento deve ser posta a série de valores almejada (ex.: E12, E24...).
// Recomendado: E12 (série encontrada comunmente nas lojas de eletrônica).
const Resistor resistors(E12);
// Tolerância para o valor da razão resistor/total.
// Se os resistores são postos em série sob uma tensão de 1V, pode-se entender
// que este valor é o erro máximo aceitável para a tensão sobre cada resistor.
// Se este valor for muito alto, o programa pode demorar muito e gerar uma
// saída poluída com resultados de erro elevado. Se for muito baixo, pode fazer
// com que não haja nenhum resultado compatível.
// Recomendado: 0.1 (funciona bem para combinações de até 5 resistores).
const double tolerance = 0.01;
// Proporção dos resistores que serão calculados.
// Este vetor pode ter qalquer quantidade de elementos.
// Nenhum elemento pode valer zero.
const vector<double> proportions{0.6, 0.6, 1.2, 1.8, 0.8};
// Ordem de grandeza da resistência total das combinações geradas.
// Se definida como 1000, serão geradas combinações com resistência total, RT,
// satisfazendo 1000 <= RT < 10000
const double magnitudeOrder = 100000;
// -------- Inputs --------



// -------- Declarações --------
vector<double> normalizedProportions;
// Vetor onde inserir os resultados.
vector<vector<double>> results;
// Recursivamente gera para cada pré-combinaçõe um novo conjunto de combinações possíveis.
void combinations (const vector<double> example);
// Gera um novo conjunto de combinações a pastir da anterior com cada valor em rangeValues.
void insertRangeValues (const vector<double> example, const vector<double> rangeValues);
// Confere um resultado e o insere em results. Também chama a função que imprime os resultados.
// Se o valor for inserido retorna true e, caso contrário, retorna false.
bool insertResult (const vector<double> newResult);
// Imprime os resultados na tela.
void printResults ();
// Retorna a soma dos elementos de um vector.
double vectorSum (const vector<double> aVector);
// Calcula as proporções de resistências normalizadas.
vector<double> normalized(const vector<double> aVector);
// Printa um vector.
string to_string (const vector<double> aVector);
// -------- Declarações --------



int main (int argc, char** argv) {

  normalizedProportions = normalized (proportions);

  #ifdef DEBUG
  cout << "Main 0: proportions = " << to_string(proportions) << endl; //debug
  cout << "Main 0: normalizedProportions = " << to_string(normalizedProportions) << endl; //debug
  #endif

  cout << endl;
  cout << "Programa para calculo de proporções de componentes que seguem as séries E." << endl
       << "Útil para projeto de divisores resistivos de tensão com resistores comenciais ou "
       << "associações com outros componentes que siguam a mesma série de valores." << endl << endl
       << "OBS.: Os valores de entrada devem ser alterados no início do código fonte do programa." << endl << endl
       << "A seguir serão apresentados os seguintes parâmetros para auxiliar a escolha das combinações de resistências:" << endl
       << "Erro RMS   -> É o valor RMS do erro nas proporções das resistências." << endl
       << "              (Quanto menor o valor, menores e mais equilibrados os valores erros)." << endl
       << "Erro ABS   -> É o valor absoluto médio dos erros nas proporções das resistências." << endl
       << "              (Quanto menor o valor, menores os valores dos erros na média)." << endl
       << "RT         -> É a soma das resistências do conjunto." << endl
       << "R1, R2,... -> São as resistências seguindo as proporções solicitadas." << endl << endl
       << "As combinações estão sendo calculadas..." << endl;

  combinations (vector<double>());
  printResults ();

  return 0;
}



// -------- Implementação de funções --------

void combinations (const vector<double> example) {

  #ifdef DEBUG
  cout << "  Comb 0: example = " << to_string(example) << endl; //debug
  #endif

  if (example.size() == 0) {

    #ifdef DEBUG
    cout << "  Comb 1: getSerieValues() = " << to_string(resistors.getSerieValues()) << endl; //debug
    #endif

    insertRangeValues (example, resistors.getSerieValues());
  }
  else if (example.size() < proportions.size()) {
    // Calculo dos valores mínimo e máximo aceitáveis para este resistor.
    unsigned refIndex = 0;
    // Valor ideal para o proximo resistor tomando como base o valor de um resistor de referência.
    double idealValue = example.at(refIndex) * (proportions.at(example.size())/proportions.at(refIndex));
    // Para o mínimo devemos considerar que cada um dos resistores anteriores poderá ter o máximo aceitável da razao resistor/total.
    // Assim devemos corrigir dividindo por (1 + tolerance) para obter o que pode ser o valor ideal
    // e multiplicando por (1 - tolerance) para encontrar o que pode ser o valor mínimo.
    double min = idealValue * ((1 - tolerance)/(1 + tolerance));
    // Analogamente devemos considerar que cada um dos resistores anteriores poderá ter o mínimo aceitável da razao resistor/total.
    // Assim devemos corrigir dividindo por (1 - tolerance) para obter o que pode ser o valor ideal
    // e multiplicando por (1 + tolerance) para encontrar o que pode ser o valor mínimo.
    double max = idealValue * ((1 + tolerance)/(1 - tolerance));

    refIndex++;

    while (refIndex < example.size()) {
      idealValue = example.at(refIndex) * (proportions.at(example.size())/proportions.at(refIndex));
      double newMin = idealValue * ((1 - tolerance)/(1 + tolerance));
      double newMax = idealValue * ((1 + tolerance)/(1 - tolerance));
      if (newMin < min) { min = newMin; }
      if (newMax > max) { max = newMax; }

      refIndex++;
    }

    #ifdef DEBUG
    cout << "  Comb 2: idealValue = " << idealValue << endl; //debug
    cout << "  Comb 2: min = " << min << endl; //debug
    cout << "  Comb 2: max = " << max << endl; //debug
    #endif

    insertRangeValues (example, resistors.getRangeValues(min, max));
  }
  else {
    #ifdef DEBUG
    cout << "  Comb 3:  tentativa de resultado -> " << to_string(example) << endl; //debug
    #endif
    insertResult (example);
  }
}

void insertRangeValues (const vector<double> example, const vector<double> rangeValues) {
  vector<double> newExample;
  unsigned index = 0;

  #ifdef DEBUG
  cout << "    insertRangeValues 0: rangeValues = " << to_string(rangeValues) << endl; //debug
  #endif

  while (index < rangeValues.size()) {
    newExample = example;
    newExample.push_back(rangeValues.at(index));
    combinations (newExample);
    index++;
  }
}

bool insertResult (const vector<double> newResult) {
  double totalResistance = vectorSum (newResult); // Soma das resistências.
  vector<double> normalizedNewResult = normalized (newResult);

  unsigned index = 0;
  double RMSerror = 0; // Valor RMS do erro das proporções de resistências.
  double ABSerror = 0; // Erro absoluto médio das proporções de resistências.

  while (index < newResult.size()) {
    double portionError = (normalizedNewResult.at(index) - normalizedProportions.at(index));
    if (portionError > tolerance) {
      return false;
    }
    RMSerror += pow(portionError, 2);
    ABSerror += abs(portionError);
    index++;
  }
  RMSerror = sqrt(RMSerror / proportions.size());
  ABSerror = ABSerror / proportions.size();

  double correction = pow (10, ceil(log10(magnitudeOrder/totalResistance)));

  vector<double> result = vector<double>();

  result.push_back(RMSerror);
  result.push_back(ABSerror);
  result.push_back(totalResistance*correction);

  index = 0;
  while (index < newResult.size()) {
    result.push_back(newResult.at(index)*correction);
    index++;
  }

  #ifdef DEBUG
  cout << "      insertResult 0: result = " << to_string(result) << endl; //debug
  #endif

  results.push_back(result);

  return true;
}

double vectorSum (const vector<double> aVector) {
  double sum = 0; // Soma das resistências
  unsigned index;

  index = 0;
  while (index < aVector.size()) {
    sum += aVector.at(index);
    index++;
  }

  return sum;
}

vector<double> normalized(const vector<double> aVector) {
  double sum = vectorSum (aVector);
  vector<double> normalized = vector<double>();
  unsigned index;

  index = 0;
  while (index < aVector.size()) {
    normalized.push_back(aVector.at(index)/sum);
    index++;
  }

  return normalized;
}

void printResults () {
  vector<double> result;
  unsigned valueWidth = 3 + ceil(log10(10*magnitudeOrder));
  unsigned i;
  unsigned j;

  cout << "Pronto!" << endl << endl;
  cout << results.size() << " resultados encontrados:" << endl << endl;

  cout << setw(10) << "Erro RMS";
  cout << setw(10) << "Erro ABS";
  cout << setw(valueWidth) << "RT";

  i = 0;
  while (i < proportions.size()) {
    cout << setw(valueWidth) << "R" + to_string(i + 1);
    i++;
  }

  cout << endl;

  i = 0;
  while (i < results.size()) {
    result = results.at(i);

    cout << setw(10) << setprecision(3) << result.at(0);
    cout << setw(10) << setprecision(3) << result.at(1);
    cout << setw(valueWidth) << setprecision(10) << result.at(2);

    j = 3;
    while (j < result.size()) {
      cout << setw(valueWidth) << result.at(j);
      j++;
    }

    cout << endl;

    i++;
  }
}

string to_string (const vector<double> aVector) {
  string vectorToString = string();
  unsigned index;

  vectorToString += "[";

  if (aVector.size() > 0) { vectorToString += to_string(aVector.at(0)); }

  index = 1;
  while (index < aVector.size()) {
    vectorToString += ", ";
    vectorToString += to_string(aVector.at(index));
    index++;
  }

  vectorToString += "]";

  return vectorToString;
}
